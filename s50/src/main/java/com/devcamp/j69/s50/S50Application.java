package com.devcamp.j69.s50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S50Application {

	public static void main(String[] args) {
		SpringApplication.run(S50Application.class, args);
	}

}
